package aty.tutoriel.proxy;

import aty.tutoriel.common.UtilDebug;

public class ClientProxy extends CommonProxy
{
    @Override
    public void registerRender() {
        UtilDebug.logDebug("== ClientProxy registerRender ==");
    }
}
