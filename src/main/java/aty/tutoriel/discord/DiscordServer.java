package aty.tutoriel.discord;

import aty.tutoriel.common.ModTutoriel;
import aty.tutoriel.common.UtilDebug;
import aty.tutoriel.common.myBlock;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class DiscordServer
{
    protected static String membres[];
    
    private static Item generateItem(String UnlocalizedName, String TextureName) {
        UtilDebug.logDebug("== DiscordServer : generateItem ==");
        UtilDebug.logDebug(UnlocalizedName);
        UtilDebug.logDebug(TextureName);
        return new Item()
                .setUnlocalizedName(UnlocalizedName)
                .setTextureName(ModTutoriel.MODID + ":" + TextureName)
                .setCreativeTab(CreativeTabs.tabMaterials);
    }
    private static Block generateBlock(String UnlocalizedName, String TextureName) {
        UtilDebug.logDebug("== DiscordServer : generateBlock ==");
        UtilDebug.logDebug(UnlocalizedName);
        UtilDebug.logDebug(TextureName);
        return new myBlock(Material.rock)
                .setBlockName(UnlocalizedName)
                .setBlockTextureName(ModTutoriel.MODID + ":" + TextureName)
                .setCreativeTab(CreativeTabs.tabMaterials);
    }
    
    public static Item generateItemPersonage(String Qui) {
        UtilDebug.logDebug("== DiscordServer : generateItemPersonage String ==");
        UtilDebug.logDebug(Qui);
        return generateItemPersonage(new Membre(Qui));
    }
    public static Item generateItemPersonage(Membre Qui) {
        UtilDebug.logDebug("== DiscordServer : generateItemPersonage Membre ==");
        UtilDebug.logDebug(Qui);
        return generateItem(Qui.getName(), Qui.getNameItem());
    }
    
    public static Block generateBlockPersonage(String Qui) {
        UtilDebug.logDebug("== DiscordServer : generateBlockPersonage String ==");
        UtilDebug.logDebug(Qui);
        return generateBlockPersonage(new Membre(Qui));
    }
    public static Block generateBlockPersonage(Membre Qui) {
        UtilDebug.logDebug("== DiscordServer : generateBlockPersonage Membre ==");
        UtilDebug.logDebug(Qui);
        return generateBlock(Qui.getName(), Qui.getNameBlock());
    }

    public static Item[] inscritToutLesMembres(Item arrayItems[]) {
        UtilDebug.logDebug("== DiscordServer : inscritToutLesMembres : Items ==");
        UtilDebug.logDebug(membres);
        UtilDebug.logDebug("membres.length " + membres.length);
        UtilDebug.logDebug(arrayItems);
        
        UtilDebug.logDebug("== aaa ==");
        Item newTab[] = new Item[membres.length];
        
        UtilDebug.logDebug("== bbb ==");
        for (Integer i=0; i<membres.length; i++) {
            
            UtilDebug.logDebug("== ccc ==");
            Membre a_member = new Membre(membres[i]);
            
            UtilDebug.logDebug("== ddd ==");
            UtilDebug.logDebug(a_member);
            
            UtilDebug.logDebug("== eee ==");
            newTab[i] = DiscordServer.generateItemPersonage(a_member);
            
            UtilDebug.logDebug("== fff ==");
            GameRegistry.registerItem(newTab[i], a_member.getNameFirstUp());
            
            UtilDebug.logDebug("== ggg ==");
        }
        UtilDebug.logDebug("== hhh ==");
        return newTab;
    }
    public static Block[] inscritToutLesMembres(Block arrayBlock[]) {
        UtilDebug.logDebug("== DiscordServer : inscritToutLesMembres : Block ==");
        UtilDebug.logDebug(membres);
        UtilDebug.logDebug("membres.length " + membres.length);
        UtilDebug.logDebug(arrayBlock);
        
        UtilDebug.logDebug("== aaa ==");
        Block newTab[] = new Block[membres.length];
        
        UtilDebug.logDebug("== bbb ==");
        for (Integer i=0; i<membres.length; i++) {
            
            UtilDebug.logDebug("== ccc ==");
            Membre a_member = new Membre(membres[i]);
            
            UtilDebug.logDebug("== ddd ==");
            UtilDebug.logDebug(a_member);
            
            UtilDebug.logDebug("== eee ==");
            newTab[i] = DiscordServer.generateBlockPersonage(a_member);
            
            UtilDebug.logDebug("== fff ==");
            GameRegistry.registerBlock(newTab[i], a_member.getNameFirstUp()+"_block");
            
            UtilDebug.logDebug("== ggg ==");
        }
        UtilDebug.logDebug("== hhh ==");
        return newTab;
    }
}
