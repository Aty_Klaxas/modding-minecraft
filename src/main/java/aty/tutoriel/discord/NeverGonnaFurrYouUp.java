package aty.tutoriel.discord;

import aty.tutoriel.common.UtilDebug;
import net.minecraft.block.Block;
import net.minecraft.item.Item;

public class NeverGonnaFurrYouUp extends DiscordServer
{
    private static String membres[] = {"alex", "aty", "aurio", "caerus", "dark", "ecu", "guirla", "gwaz", "maxou", "orange", "step", "paw"};
    //private static String membres[] = {"aurio", "ecu"};
    public static Item[] inscritToutLesMembres(Item arrayItems[]) {
        UtilDebug.logDebug("== NeverGonnaFurrYouUp : inscritToutLesMembres : Item ==");
        DiscordServer.membres = NeverGonnaFurrYouUp.membres;
        return DiscordServer.inscritToutLesMembres(arrayItems);
    }
    public static Block[] inscritToutLesMembres(Block arrayBlock[]) {
        UtilDebug.logDebug("== NeverGonnaFurrYouUp : inscritToutLesMembres : Block ==");
        DiscordServer.membres = NeverGonnaFurrYouUp.membres;
        return DiscordServer.inscritToutLesMembres(arrayBlock);
    }
}
