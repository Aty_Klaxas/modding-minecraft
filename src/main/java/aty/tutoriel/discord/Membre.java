package aty.tutoriel.discord;

public class Membre
{
    private String name;
    public Membre()                { name = null;     }
    public Membre(String prm_name) { name = prm_name; }
    
    public String getName()        { return name; }
    public String getNameFirstUp() { return name.substring(0, 1).toUpperCase() + name.substring(1); }
    public String getNameItem()    { return "item_" + name; }
    public String getNameBlock()   { return "block_" + name; }
    public Membre setName(String name) {  this.name = name; return this; }
    
    public String toString() { return name; }
}
