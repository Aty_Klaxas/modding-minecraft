package aty.tutoriel.common;

import aty.tutoriel.discord.NeverGonnaFurrYouUp;
import aty.tutoriel.proxy.CommonProxy;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;

@Mod(modid = ModTutoriel.MODID, name = "Mod Aty Tutoriel", version = "0.0.2")

public class ModTutoriel
{
    public static final String MODID = "modtutoriel";

    @Instance(MODID)
    public static ModTutoriel instance;

    @SidedProxy(clientSide = "aty.tutoriel.proxy.ClientProxy", serverSide = "aty.tutoriel.proxy.CommonProxy")
    public static CommonProxy proxy;

    public static Item mesItemPerso[], itemTuto_1, itemTuto_2;
    public static Block mesBlockPerso[], blocTuto_1, blocTuto_2;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        UtilDebug.logDebug("== preInit ==");
        mesItemPerso = NeverGonnaFurrYouUp.inscritToutLesMembres(mesItemPerso);
        mesBlockPerso = NeverGonnaFurrYouUp.inscritToutLesMembres(mesBlockPerso);

        blocTuto_1 = new myBlock(Material.rock).setBlockName("tutoriel_1").setBlockTextureName(MODID + ":block_tutoriel_1").setCreativeTab(CreativeTabs.tabMaterials);
        blocTuto_2 = new myBlock(Material.wood).setBlockName("tutoriel_2").setBlockTextureName(MODID + ":block_tutoriel_2").setCreativeTab(CreativeTabs.tabMaterials);

        GameRegistry.registerBlock(blocTuto_1, "mon_block_custom");
        GameRegistry.registerBlock(blocTuto_2, ItemBlock.class, "mon_block_custom2");

        UtilDebug.logDebug("== FIN preInit ==");
    }

    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        UtilDebug.logDebug("== init ==");
        proxy.registerRender();
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
        UtilDebug.logDebug("== postInit ==");
    }

}
