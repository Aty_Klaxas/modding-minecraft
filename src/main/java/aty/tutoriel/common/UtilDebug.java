package aty.tutoriel.common;

public class UtilDebug
{
    private static boolean debug = true;

    private static void run(String prm_debug) {
        if (debug) {
            System.out.println("======== " + prm_debug + " ========");
        }
    }

    public static void logDebug(java.lang.Object prm_debug) {
        if (prm_debug == null) {
            run("null");
        } else {
            run( prm_debug.toString() );
        }
    }

    public static void logDebug(String prm_debug) {
        run(prm_debug);
    }
}
