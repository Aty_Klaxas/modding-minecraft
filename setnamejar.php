<?php

require_once 'config.php';
require_once 'functions.php';

$params = json_decode($argv[1], true);

$informations = [
    'git_sha_short' => $params['shashort'],
    'git_project_name' => $params['project_name'],
    'config_version' => $configs['version'],
    'config_MCversion' => $configs['MCversion'],
    'config_mod_name' => $configs['mod_name'],
];

echo "definition des variables et nom dans le build.gradle" . PHP_EOL;

echo "Nom de la branche: ".$informations['git_project_name'].PHP_EOL;
echo "Nom du mod: ".$informations['config_mod_name'].PHP_EOL;
echo "MC Version: ".$informations['config_MCversion'].PHP_EOL;
echo "Version: ".$informations['config_version'].PHP_EOL;
echo "Commit: ".$informations['git_sha_short'].PHP_EOL;

replace_value('build.gradle', '{', '}', [
    'archivesBaseName' => $informations['config_mod_name'],
    'version' => $informations['config_MCversion'] . "-" . $informations['config_version'] . "-" . $informations['git_project_name'] . "-" . $informations['git_sha_short'],
]);

echo "Replacement des valeurs de debug en False" . PHP_EOL;

replace_value_simple('src/main/java/aty/tutoriel/common/UtilDebug.java', [
    'private static boolean debug = ' => 'false;',
]);

