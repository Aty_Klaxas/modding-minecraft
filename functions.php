<?php

function cat_file($filename) {
    foreach (explode("\n", file_get_contents($filename)) as $value) {
        echo $value . PHP_EOL;
    }
}

function replace_value_simple($filename, $arrayFields, $debug = false) {
    $fileString = file_get_contents($filename);
    $fileArray = explode("\n", $fileString);

    foreach ($arrayFields as $find => $replace) {
        foreach ($fileArray as $noLine => &$line) {
            if (stristr(trim($line), $find) !== false) {
                $nbSpace = explode(' ', $line);
                $cpt = 0;
                while ($nbSpace[$cpt] == "" && $cpt < count($nbSpace)) {
                    $cpt++;
                }
                $newStr = "";
                for ($u=0; $u < $cpt; $u++) { 
                    $newStr .= " ";
                }
                $newStr .= $find . $replace;
                $line = $newStr;
            }
        }
    }

    array_to_string($filename, $fileArray);
}

function replace_value($filename, $opener, $closer, $arrayFields, $fielder = '"') {
    $fileString = file_get_contents($filename);
    $fileArray = explode("\n", $fileString);

    foreach ($arrayFields as $key => $value_2) {
        $explodeKey = explode('.', $key);
        $explodeKeyLast = array_pop($explodeKey);

        $contener = [];
        $lineBefore = "";

        foreach ($fileArray as $noLine => &$value) {
            if ( substr(trim($value), 0, 2) != '//' ) {
                $exempleKey = array_merge($contener, [$explodeKeyLast]);
                $exempleKey = implode('.', $exempleKey);
                $PSvalue = " $value ";

                $x = explode($opener, $PSvalue);
                if (count($x) == 2) {
                    if (empty(trim($x[0]))) {
                        array_push($contener, trim($lineBefore));
                    } else {
                        array_push($contener, trim($x[0]));
                    }
                }

                $x = explode($closer, $PSvalue);
                if (count($x) == 2) {
                    $poped = array_pop($contener);
                }

                if ( !empty($explodeKeyLast) ) {
                    $x = explode($explodeKeyLast, $PSvalue);
                } else {
                    $x = null;
                }
                if ( !empty($x) ) {
                    if (count($x) == 2 && $exempleKey == $key) {
                        $exp = explode('"', $PSvalue);
                        $exp[1] = $value_2;
                        $PSvalue = implode('"', $exp);
                    }
                }

                $lineBefore = $PSvalue;

                $PSvalue = substr($PSvalue, 1);
                $PSvalue = substr($PSvalue, 0, strlen($PSvalue) - 1);

                if ( empty(trim($PSvalue)) ) {
                    $PSvalue = "\n";
                }

                $value = $PSvalue;
            }
        }

    }

    array_to_string($filename, $fileArray);
}

function array_to_string($filename, $array) {
    $fileStringOut = "";
    for ($i=0; $i < count($array)-1; $i++) {
        if ( !empty(trim($array[$i])) ) {
            $nbSpace = explode(' ', $array[$i]);
            $cpt = 0;
            while ($nbSpace[$cpt] == "" && $cpt < count($nbSpace)) {
                $cpt++;
            }
            $newStr = "";
            for ($u=0; $u < $cpt; $u++) { 
                $newStr .= " ";
            }
            $newStr .= trim($array[$i]);
            $fileStringOut .= $newStr;
        }
        $fileStringOut .= "\n";
    }
    file_put_contents($filename, $fileStringOut);
}